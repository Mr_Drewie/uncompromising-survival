ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WEBBER
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WEBBER.DESCRIBE

--	[ 		Webber Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Mister Wilson says that isn't how ghosts work."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Father told me not to play with machinery."
    ANNOUNCE.ANNOUNCE_RATRAID = "What's that sound? Friends?"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Aww, it came out to play!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Hey, wait up! Let's play tag!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Ack! Owie!",
        "Rain, rain, go away!",
        "Ow! Our fur's getting singed!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Mushroom, mushroom!"

    DESCRIBE.UNCOMPROMISING_RAT = "Nice fur!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "It's looking at us! Hey there little guy!"
    DESCRIBE.RATPOISONBOTTLE = "Wendy really likes this stuff! Wonder what it tastes like..."
    DESCRIBE.RATPOISON = "Whoops! It's all spilled!"

    DESCRIBE.MONSTERSMALLMEAT = "We'd rather not think about that too much."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "It'll tide us over."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "It's really chewy."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Is that where the yucky rain came from?"
    DESCRIBE.TOADLING = "That's one weird frog."
	
	DESCRIBE.GASMASK = "Now we won't cough so much."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Oh no! Are you okay?"
	ANNOUNCE.ANNOUNCE_SNEEZE = "a...choooOOOOOOO!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Sniff. Sniff. We must have allergies."
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Wow! I think our allergies are gone!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"W... We don't like the sound of that!",
		"The sky is falling! The sky is falling!",
		"Watch out!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "We're stuck! Help!"
	DESCRIBE.SAND = "It's nice to feel it beneath our many feet."
	DESCRIBE.SANDHILL = "Wish we had a bucket and a shovel."
	DESCRIBE.SNOWPILE = "I wish it was a good time to play..."
	DESCRIBE.SNOWGOGGLES = "We are neither cat eyed or bug eyed."
	
	DESCRIBE.SNOWMONG = "4 eyes?! Definitely not our friend!"
	DESCRIBE.SHOCKWORM = "Freaky frilly!"
	DESCRIBE.ZASPBERRY = "Zappy!"
	DESCRIBE.ICEBOOMERANG = "Brrrr, it's chilly just to have near us!"
	DESCRIBE.SNOWBALL_THROWABLE = "Ooh! Ooh! Who wants to snowball fight?"
	DESCRIBE.VETERANSHRINE = "It's looking at us!!"

	DESCRIBE.BUSHCRAB = "Not one of our friends!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "It sticks and stains our fur."