ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WOLFGANG
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WOLFGANG.DESCRIBE

--	[ 		Wolfgang Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Heart not stay in spooky ghost. Where body?"
    ANNOUNCE.ANNOUNCE_WINONAGEN = "How do Wolfgang make box work?"
    ANNOUNCE.ANNOUNCE_RATRAID = "Wolfgang hear tiny squeak squeaks?"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Is big rats!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "No! Come back! Bad rat! Bad rat!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Rain is burning Wolfgang's muscles!",
        "Rain is biting Wolfgang!",
        "Rain is punch! Where Wolfgang punch back?",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Ahh! Mushroomy trees?"

    DESCRIBE.UNCOMPROMISING_RAT = "Is greedy little hair puff!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "Beady eye hole make Wolfgang uncomfortable."
    DESCRIBE.RATPOISONBOTTLE = "Icky death soup."
    DESCRIBE.RATPOISON = "Wolfgang not have to catch rat if rat dead! Hah hah!"

    DESCRIBE.MONSTERSMALLMEAT = "Look bad. Smell bad. Is bad?"
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Still look not good. Wolfgang go find better meat?"
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Small bad meat is rock now."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Smelly mushroom make rain hurt!"
    DESCRIBE.TOADLING = "Little frog not look right."
	
    DESCRIBE.GASMASK = "Is good for Wolfgang lungs."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Big scary spider is dead!"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Achoo! Excuse Wolfgang."
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Allergies strong like Wolfgang!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Ha! Wolfgang beat allergies!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Wolfgang not like scary screeches!",
		"Scary monster angry!",
		"Fire! Fire!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "Too tough, Wolfgang cannot break!"
	DESCRIBE.SAND = "When Wolfgang holds it it slips through his large, strong hands."
	DESCRIBE.SANDHILL = "Tiny, crushable hill."
	DESCRIBE.SNOWPILE = "Cold is no good for the muscles!."
	DESCRIBE.SNOWGOGGLES = "Is... little tight... for head."
	
	DESCRIBE.SNOWMONG = "Snow is fighting back!"
	DESCRIBE.SHOCKWORM = "Wolfgang shocked!"
	DESCRIBE.ZASPBERRY = "Sparky berry!"
	DESCRIBE.ICEBOOMERANG = "Little ice stick too cold to hold!"
	DESCRIBE.SNOWBALL_THROWABLE = "Is fun game!"
	DESCRIBE.VETERANSHRINE = "Wolfgang don't like looking at head."

	DESCRIBE.BUSHCRAB = "Just wanted food!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "Babushka used to make soup for Wolfgang when sick."