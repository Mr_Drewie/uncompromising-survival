ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WENDY
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WENDY.DESCRIBE

--	[ 		Wendy Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Were it so easy..."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "I'd probably ruin it, like everything else..."
    ANNOUNCE.ANNOUNCE_RATRAID = "Oh joy."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Pests, and pestilence!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Hey! Those are my burdens to bear!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "And their flesh will fall away...",
        "The rain is breaking me down...",
        "This is nature's punishment...",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "A curse from the bowels of the earth!"

    DESCRIBE.UNCOMPROMISING_RAT = "Wreched daemons wrought to topple the pillars of vanity."
    DESCRIBE.UNCOMPROMISING_RATHOLD = "But one mouth of a den of thieves."
    DESCRIBE.RATPOISONBOTTLE = "There's a thought..."
    DESCRIBE.RATPOISON = "Even if I were to partake, this world would drag me back..."

    DESCRIBE.MONSTERSMALLMEAT = "A small, dark well of condensed agony..."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Small, filled with pain, and ready to be consumed by this world..."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Some weight has lifted from it, but it will never truly escape its nature..."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "A parasitic existence feeding upon the very earth."
    DESCRIBE.TOADLING = "They're very loyal to their kind..."
	
	DESCRIBE.GASMASK = "It filters air but not despair."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "An inevitable end."
	ANNOUNCE.ANNOUNCE_SNEEZE = "achoo! achoo! sniff."
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Ugh. Even my sinuses are suffering."
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "My sinuses are clear, but suffering continues."
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Those cries are familiar.",
		"I hope it will be a swift end.",
		"Some just want to watch the world burn.",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "You cannot hold me forever."
	DESCRIBE.SAND = "I wonder if the sand knows of its own insignificance?"
	DESCRIBE.SANDHILL = "This looks impermanent."
	DESCRIBE.SNOWPILE = "Cold death awaits us."
	DESCRIBE.SNOWGOGGLES = "Like tears, in the snow."
	
	DESCRIBE.SNOWMONG = "Just another cruel joke."
	DESCRIBE.SHOCKWORM = "A light in the darkness."
	DESCRIBE.ZASPBERRY = "Perhaps it will bring some light to my life. Probably not."
	DESCRIBE.ICEBOOMERANG = "Ripped off the tail of a maggot."
	DESCRIBE.SNOWBALL_THROWABLE = "I remember playing in the snow with Abigail."
	DESCRIBE.VETERANSHRINE = "I am used to pain."

	DESCRIBE.BUSHCRAB = "Another cruel joke from mother nature."
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "I wish I could bring Abigail back from death."