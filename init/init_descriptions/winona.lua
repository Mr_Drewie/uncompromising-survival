ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WINONA
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WINONA.DESCRIBE

--	[ 		Winona Descriptions		]   --

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Without the body? That wouldn't be too constructive."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Even if it's small, please...p-put your bug report in my Discord server, Player-san!"
    ANNOUNCE.ANNOUNCE_RATRAID = "Aw, rats."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "There they are! Get em!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Hey, they're runnin' off with my gear!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "The weather's fixing to burn me out!",
        "This stuff's gonna burn through my clothes! And me!",
        "I need to get outta this rain!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Somethin's building up down there!"

    DESCRIBE.UNCOMPROMISING_RAT = "That's one part of home I was hoping NOT to see again."
    DESCRIBE.UNCOMPROMISING_RATHOLD = "Seems you've been ratted out!"
    DESCRIBE.RATPOISONBOTTLE = "Darn, this stuff woulda put Red Squill to shame."
    DESCRIBE.RATPOISON = "Nice not having to worry about rat presents all over our stuff."

    DESCRIBE.MONSTERSMALLMEAT = "Not too sure why I wanted this..."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Does cooking something like this make it food?"
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Old, sad anger meat."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "That can't be good. We need to cut it off at the source!"
    DESCRIBE.TOADLING = "Well THAT one's got an an attitude."
	
    DESCRIBE.GASMASK = "Proper equipment for a mining job."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Do we really need to keep that around?"	
	ANNOUNCE.ANNOUNCE_SNEEZE = "ACHOO! Whoops!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Something is dancin' in my nose!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "My nose is finally getting a break!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"That guys working up a sweat with all that yelling.",
		"Jeez! Cool it!",
		"Get your hard hats on!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "I'm caught!"
	DESCRIBE.SAND = "I can only break it down so much."
	DESCRIBE.SANDHILL = "I usually leave the cleanup to the janitors."
	DESCRIBE.SNOWPILE = "Save your back, shovel early."
	DESCRIBE.SNOWGOGGLES = "Winter workin."
	
	DESCRIBE.SNOWMONG = "Is that one of those snow-mole-bile things I've heard about?"
	DESCRIBE.SHOCKWORM = "That's a saftey hazard if I've ever seen one!"
	DESCRIBE.ZASPBERRY = "May as well taste it."
	DESCRIBE.ICEBOOMERANG = "Quite a handy tool, ain't it?"
	DESCRIBE.VETERANSHRINE = "This doesn't look like Charlie's doing."
	DESCRIBE.SNOWBALL_THROWABLE = "Maybe I could load it in a catapult? Hmm, nah."

	DESCRIBE.BUSHCRAB = "Thats some dangerous weeding!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "We'll get those plants workin' like a well oiled machine!"