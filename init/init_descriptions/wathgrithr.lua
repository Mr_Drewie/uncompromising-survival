ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WATHGRITHR
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE

--	[ 		Wigfrid Descriptions		]   --

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Comrade! We must venture to thine corpse!"
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Such is best left to the mechanical maiden."
    ANNOUNCE.ANNOUNCE_RATRAID = "Something squeaky approaches."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Protect the reliquaries!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "The dastards flee with our bounty!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "The very skies rain their fury upon me!",
        "Singeing droplets, like razors against mine skin!",
        "Freyr's wrath showers upon us!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "A blight springs up from the depths of Hel!"

    DESCRIBE.UNCOMPROMISING_RAT = "And thus cometh a plague upon both our houses!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "Den of the little beasts!"
    DESCRIBE.RATPOISONBOTTLE = "My treasures, the bait. Mine fury, the trap."
    DESCRIBE.RATPOISON = "Emerge little plunderers, and face the end of days!"

    DESCRIBE.MONSTERSMALLMEAT = "Meat of tiny evil."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Might I partake of such dark omens?"
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Mine grim provisions."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Let us rip out this affliction by the root!"
    DESCRIBE.TOADLING = "Fel guardians of corruption!"
	
	DESCRIBE.GASMASK = "Poisonous air will not consume me!!"
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Another victory!"
	ANNOUNCE.ANNOUNCE_SNEEZE = "aaAAAH...cho!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Mine nostrils chafe with itchiness!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "The smoke has cleared!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Hel's fury is upon us!",
		"Ragnarok has come!",
		"The beast roars loud!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "A cheap trick! Fight with honor!"
	DESCRIBE.SAND = "You are a very clean dirt."
	DESCRIBE.SANDHILL = "I will not make a mountain out of a sandhill."
	DESCRIBE.SNOWPILE = "Skaði would not be impressed."
	DESCRIBE.SNOWGOGGLES = "We shall fight the storm itself!"
	
	DESCRIBE.SNOWMONG = "Begone, vile snowmonger!"
	DESCRIBE.SHOCKWORM = "A vicious counter attack!"
	DESCRIBE.ZASPBERRY = "Could Thor's power truly be contained in a wild fruit?"
	DESCRIBE.ICEBOOMERANG = "No one can run from my spear!"
	DESCRIBE.VETERANSHRINE = "No challenge is above me!"
	DESCRIBE.SNOWBALL_THROWABLE = "Aim betwixt the eyes!"

	DESCRIBE.BUSHCRAB = "A wild encounter!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "A weapon for fighting back the plagues!"