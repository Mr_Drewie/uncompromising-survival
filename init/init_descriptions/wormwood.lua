ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WORMWOOD
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WORMWOOD.DESCRIBE

--	[ 		Wormwood Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Goes in meat parts"
    ANNOUNCE.ANNOUNCE_WINONAGEN = "How do make go?"
    ANNOUNCE.ANNOUNCE_RATRAID = "Squeak!"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Stealer squeak sqeaks!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Squeaky go home?"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Water hurt?!",
        "Water burn!",
        "Why water bad?!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Friends? Sick friends! No! Bad!"

    DESCRIBE.UNCOMPROMISING_RAT = "Hairy stuff stealer"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "Squeaker hole"
    DESCRIBE.RATPOISONBOTTLE = "Friend hat soup!"
    DESCRIBE.RATPOISON = "Smell good"

    DESCRIBE.MONSTERSMALLMEAT = "Not angry now"
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Weird belly stuff"
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Chewy!"

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Friend, please stop"
    DESCRIBE.TOADLING = "Friend ribbit?"
	
    DESCRIBE.GASMASK = "Breather with Tweeter clothes"
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Sleeping?"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Hiccup!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Happy time is coming!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Oh. Happy time gone"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Scary!",
		"Fire! Hurt friends!",
		"Not friend! Not friend!!!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "Bad hug!"
	DESCRIBE.SAND = "Dirt"
	DESCRIBE.SANDHILL = "Pile of dirt"
	DESCRIBE.SNOWPILE = "Friend killer!"
	DESCRIBE.SNOWGOGGLES = "Kitty eyes?"
	
	DESCRIBE.SNOWMONG = "Cold Digger!"
	DESCRIBE.SHOCKWORM = "Zzzzt Wiggler!"
	DESCRIBE.ZASPBERRY = "Zzzzt Food"
	DESCRIBE.ICEBOOMERANG = "Make things stop"
	DESCRIBE.VETERANSHRINE = "Sad head?"
	DESCRIBE.SNOWBALL_THROWABLE = "Fun?"

	DESCRIBE.BUSHCRAB = "Friend? Yes... no?"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "Cure friends!"