ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WARLY
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WARLY.DESCRIBE

--	[ 		Warly Descriptions		]   --

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "It would be best served to the patron themself."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Engineering isn't part of my palate."
    ANNOUNCE.ANNOUNCE_RATRAID = "Oh non..."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Get the mort aux rats!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Crapules! They're getting away!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "No no, this weather is going to ruin my finery!",
        "Ow! What is with this rain?",
        "Ah! I'm being sautéed!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Ahh! It's a buried buffet!"

    DESCRIBE.UNCOMPROMISING_RAT = "Don't let the filthy little pestes near my kitchen!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "A trouble trou!"
    DESCRIBE.RATPOISONBOTTLE = "Peace of mind in a bottle."
    DESCRIBE.RATPOISON = "Dinner is served..."

    DESCRIBE.MONSTERSMALLMEAT = "A delicacy in the works?"
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Mm, how to save this dish...?"
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "I hate to throw away good food, but...this isn't."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "What an unsavory sprout."
    DESCRIBE.TOADLING = "Seems they don't stray far from that fungi's strange aroma."
	
    DESCRIBE.GASMASK = "I could use this in the smoke house."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Mon dieu! That must get rid of!"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Atchoum!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Ugh! My allergies!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "I think... my nose has cleared!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"It's raining fiery meatballs!",
		"The sky is falling!",
		"Cats and dogs! Great flaming cats and dogs!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "Definetly not a spice root!"
	DESCRIBE.SAND = "Lots of tiny, tiny stones."
	DESCRIBE.SANDHILL = "The sand has formed a small pile."
	DESCRIBE.SNOWPILE = "Snow is a new thing for me, seems like a problem."
	DESCRIBE.SNOWGOGGLES = "Still not my style, but I can see better atleast."
	
	DESCRIBE.SNOWMONG = "This is no frozen treat!"
	DESCRIBE.SHOCKWORM = "Touching it could cook me to perfection!"
	DESCRIBE.ZASPBERRY = "Perhaps a nice raspberry parfait?"
	DESCRIBE.ICEBOOMERANG = "Maybe if I dip it in flavor, and save it for Summer?"
	DESCRIBE.SNOWBALL_THROWABLE = "I wasn't ever able to play with snow; I never saw any where I'm from."
	DESCRIBE.VETERANSHRINE = "The lengths I go for ingredients..."

	DESCRIBE.BUSHCRAB = "Berries and crab cake, anyone? No?"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "I doubt this would make for a good cooking oil."