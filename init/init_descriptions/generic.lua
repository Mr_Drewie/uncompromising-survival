-- Wilson's speech file
-- The strings here are also used when other characters are missing a line
-- If you've added an object to the mod, this is where to add placeholder strings
-- Keep things organized

ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.GENERIC
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE

--	[ 		Wilson Descriptions		]   --

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Hearts aren't part of ghost anatomy!"
    ANNOUNCE.ANNOUNCE_WINONAGEN = "That's not really my thing."
    ANNOUNCE.ANNOUNCE_RATRAID = "Squeak squeak?"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "It's a rat-at-tack!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Hey, my stuff!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "The rain, it burns!",
        "Ack, acid rain!",
        "I need shelter!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "There's fungus among us!"

    DESCRIBE.UNCOMPROMISING_RAT = "They're rataliating!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "It must lead to their labo-rat-ory."
    DESCRIBE.RATPOISONBOTTLE = "It's labeled \"Do not drink. That means you Webber.\""
    DESCRIBE.RATPOISON = "It's all murdery."

    DESCRIBE.MONSTERSMALLMEAT = "Small, angry meat."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "...it's still purple meat."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "What a little jerk."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "There's the source of the pestilence!"
    DESCRIBE.TOADLING = "It sure likes those weird trees."
	DESCRIBE.TOAD = "That toads looking rather sick."
	
    DESCRIBE.GASMASK = "Now I can breath anywhere."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "That's really gross."
	ANNOUNCE.ANNOUNCE_SNEEZE = "AHHH CHOOOO!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "My nose feels.. itchy."
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "I don't want to claw my eyes out anymore."
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"It sure is heating up around here.",
		"Geez, that guys getting loud!",
		"I've got a bad feeling about this.",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "I'm stuck!"
	DESCRIBE.SAND = "A handy pile of pocket sand."
	DESCRIBE.SANDHILL = "You better stay out of my shoes."
	DESCRIBE.SNOWPILE = "That'll be a problem if it builds up."
	DESCRIBE.SNOWGOGGLES = "Icy clearly now!"
	
	DESCRIBE.SNOWMONG = "Shiverbug!"
	DESCRIBE.SHOCKWORM = "Quite the conductive worm!"
	DESCRIBE.ZASPBERRY = "I can feel the electricity flowing through it."
	DESCRIBE.ICEBOOMERANG = "It has a chilling bite to it."
	
	DESCRIBE.MINOTAUR_BOULDER = "Thats a nice boulder."
	DESCRIBE.MINOTAUR_BOULDER_BIG = "Thats a nice BIG boulder."
	DESCRIBE.BUSHCRAB = "AH! How long was he down there?!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "Finally, a practical use for the moons transformative properties."
	DESCRIBE.TOADLINGSPAWNER = "Uh oh."
	DESCRIBE.SNOWBALL_THROWABLE = "Not the face!"
	DESCRIBE.VETERANSHRINE = "I know what I'm doing... right?"
	

--	[ 		Recipe Descriptions		]   --

RECIPE_DESC = GLOBAL.STRINGS.RECIPE_DESC

    RECIPE_DESC.RAT_BURROW = "A den of annoying little hairballs."
    RECIPE_DESC.RATPOISON = "A most deadly feast."
    RECIPE_DESC.GASMASK = "Makes everything smell like bird."
	