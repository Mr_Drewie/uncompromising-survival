ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WILLOW
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WILLOW.DESCRIBE

--	[ 		Willow Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Wait, where would I even put this thing?"
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Eww, I don't want grease on my hands!"
    ANNOUNCE.ANNOUNCE_RATRAID = "I really don't like that sound..."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Ugh, why did it have to be rats?!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Hey, my stuff! Get back here!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Ow, ow, ow!",
        "Water isn't supposed to burn!",
        "Ow! The feeling's mutual, water!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Woah! That's a thing!"

    DESCRIBE.UNCOMPROMISING_RAT = "Stay away from Bernie!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "Eugh. Can we just smash it shut?"
    DESCRIBE.RATPOISONBOTTLE = "Yea, this'll take care of those gross little jerks!"
    DESCRIBE.RATPOISON = "Cooome and get iiiiit..."

    DESCRIBE.MONSTERSMALLMEAT = "It's from gross little hair monsters..."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "I don't think I can burn all the hairs out..."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Now it's just gross AND old."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Ew! Burn it!"
    DESCRIBE.TOADLING = "Gross! It's all sticky-looking."
	
    DESCRIBE.GASMASK = "Cuts down on the coughing."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Burn it, burn it! Ew ew ew!"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Achoo!!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "My sinuses are burning."
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Sniff! I feel better now!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"YES! BURN!",
		"I WANT TO SET THE WORLD ON FIRE!",
		"FIRE IN THE SKY! YES!!!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "Oh, you're really asking to be burned!"
	DESCRIBE.SAND = "Big deal, it's sand."
	DESCRIBE.SANDHILL = "Oh, look! It's some sand."
	DESCRIBE.SNOWPILE = "Fire seems oddly ineffective."
	DESCRIBE.SNOWGOGGLES = "Now I can see the opposite of the world burning."
	
	DESCRIBE.SNOWMONG = "Melt that sucker!"
	DESCRIBE.SHOCKWORM = "It's so frilly!"
	DESCRIBE.ZASPBERRY = "I'm not blowing any raspberries."
	DESCRIBE.ICEBOOMERANG = "I mean, I guess I can use it."
	DESCRIBE.SNOWBALL_THROWABLE = "Ugh, snow is boring! Let's throw fireballs instead!"
	DESCRIBE.VETERANSHRINE = "Ew, it's covered in that gunk."

	DESCRIBE.BUSHCRAB = "Should have burned it down... always should have burned it down!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "I would have settled for burning the diseased plants down."