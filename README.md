![Uncompromising Logo](images/logo.png)

# Don't Starve Together - Uncompromising Mode Collab

[Steam Workshop Mod Page Link](https://steamcommunity.com/sharedfiles/filedetails/?id=2013783736)

[Discord Server Link](https://discord.gg/UF7FKpn)

[Steam Group](https://steamcommunity.com/groups/UncompromisingDST)

[Klei Forum Link](https://forums.kleientertainment.com/forums/topic/111892-announcement-uncompromising-mode/)

[Mod Wiki](https://gitlab.com/uncompromising-survival/uncompromising-survival/wikis/home)



## Description

This is the mod repository for the Uncompromising Mode Community Collab.

The aim of this is to make the game harder for veteran players, while also channeling the wisdom and modding talent of the DST community.



## How to Install

Download the .zip from this folder, and unzip it in your mod folders, for eg.

C:\Program Files (x86)\Steam2\steamapps\common\Don't Starve Together\mods\

Host a server and enable the mod in your world



## Current Features & Planned Changes

Check [CHANGELOG.md](https://gitlab.com/uncompromising-survival/uncompromising-survival/blob/master/CHANGELOG.md)



## Mod Collab Change Guidelines

```
1  - Preserve as much as possible the core DST mechanics (the game should still feel like vanilla DST).
2  - Avoid introducing new characters or biomes.
3  - Increase the overall difficulty of the game, while being fair and avoiding tedium or repetitive actions.
4  - Emphasize skill in the design.
5  - Tune and tweak all mechanics that do not fit the Uncompromising theme.
6  - Changes should be visible from in-game and clear, without needing to consult a changelist.
7  - Tailor changes for Veteran players.
8  - Focus on replicating a similar Don't Starve experience as the first time we played this game and everything was new, but for veterans.
9  - Changes must be community driven and voted by the community and approved by the Uncompromising team contributors collectively.
10 - Keep things lore friendly. All changes must fit in the Klei Don't Starve universe.
```


## Collaboration

All suggestions must go through a community voting process.

More details on the process in the Welcome section of [discord](https://discord.gg/UF7FKpn).

Join the discord server if you wish to collaborate as a dev, artist or tester.

You can also join simply to discuss, suggest and vote on ideas.


## How to contribute code

Recommended Git Client: [GitHub Desktop](https://desktop.github.com/) (Works with Gitlab)

Recommended Code Editor: [Visual Studio Code](https://code.visualstudio.com/)

Basic dev process is:
```
1. Create a Gitlab account
2. Contact one of the maintainers of this project, to add you as dev
3. Download Github Desktop (or any git client you prefer)
4. Clone the repository so you have it locally
   - I recommend cloning it in your <DST>/mods folder, for fast prototyping
5. In Github desktop clieant login to gitlab (this downloads the code locally)
6. To make changes, create a new branch (this will be where you will work)
7. Publish your branch (this makes it available publicly here on gitlab)
8. Do changes on your branch, or copy files inside the local repository location
9. Github desktop will see your changes, and you have to commit them
   - Add descriptive messages for your commits
   - Best practice is to keep one functionality for one commit 
10.Push your changes (this makes them appear on your branch, publicly)
11.On gitlab.com go to our Project > Merge Requests (here you can create requests to merge your code into our master)
12.Submit a merge request from your own branch, to merge into the master branch
13.Make sure you fulfil the checklists that appear when you do so
14.After that, code maintainers will check your merge request
   - If any issues are in the code, we will request a change, and you can create it and push it on your branc
15.If all is good, code maintainers will approve and merge your changes.

Note: If you want to work on other changes while waiting for approval, create a different branch, and work on that one.

Congratulations, you just helped the Uncompromising Mode Collab!
```

## Credits

Check [CONTRIBUTING.md](https://gitlab.com/uncompromising-survival/uncompromising-survival/blob/master/CONTRIBUTING.md)